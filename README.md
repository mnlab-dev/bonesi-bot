# README #

Information on how to install `bonesi`, `sqlmap` and run the bot which will make use of them...

## How to install `bonesi`:

```bash
# prerequisites
sudo apt-get install build-essential
sudo apt-get install autoconf
sudo apt-get install libpcap-dev
sudo apt-get install libnet1-dev
sudo apt-get install git

# source
git clone http://github.com/Markus-Go/bonesi.git
cd bonesi/

# build
sudo autoreconf -f -i
sudo ./configure
sudo make
sudo make install

# test, will print usage info...
bonesi
```

## How to install `sqlmap`:

```bash
git clone --depth 1 https://github.com/sqlmapproject/sqlmap.git sqlmap-dev
```

## How to install `bot`:

```bash
git clone https://bitbucket.org/mnlab-dev/bonesi-bot.git
sudo apt-get install python3-pip
sudo pip3 install socketIO_client
```

## How to run:

**make sure the following run with** `sudo`

```bash
# without parameters, (uses default: 127.0.0.1 5000)
python3 bonesi-bot.py

# with parameters
python3 bonesi-bot.py [C&C_ip] [C&C_port]
```

## SQL injection test environment

[http://github.com/sqlmapproject/testenv.git](http://github.com/sqlmapproject/testenv.git)

```bash
# prerequisites
# docker:
sudo apt-get remove docker docker-engine docker.io
sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual
curl -fsSL get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker ubuntu
# docker-compose:
sudo curl -L https://github.com/docker/compose/releases/download/1.17.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# test environment
git clone http://github.com/sqlmapproject/testenv.git
cd testenv/docker/

# run
docker-compose up
```

