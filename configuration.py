import os
import configparser
import getpass


class config:

    # --------------------------  Global Variables  -----------------------------
    # Base Directory
    BASE_DIR = os.path.dirname(__file__)
    BASE_USER_DIR = os.path.join("/home", getpass.getuser())

    # Configuration File Directory
    CONF_FILE_DIR = os.path.join(BASE_DIR, "main.conf")

    config = configparser.ConfigParser()
    config.read(CONF_FILE_DIR)

    # --------------------------  Default Variables  ----------------------------
    
    INTERFACE = config['DEFAULT']['interface']
    IP = config['DEFAULT']['ip']
    PORT = int(config['DEFAULT']['port'])
    MOBILE = config['DEFAULT']['mobile']
